#Code Examples for CPR Clients

##0. Running the Examples

The Java FTP example has a dependency on the Apache Commons Net library
which needs to be downloaded separately, from: https://commons.apache.org/proper/commons-net/

To run the Java example, you first need to compile the program. When compiling the
example, you must include the Commons Net JAR file in the compiler's classpath, e.g.:

    javac -classpath commons-net-x.y.jar CPRFTP.java

Once compiled, you can run the example on the command prompt using one of:

    java -classpath commons-net-x.y.jar;. CPRFTP <ftpuser> <password>
    java -classpath commons-net-x.y.jar;. CPRFTP parse <exportfile>
    java -classpath commons-net-x.y.jar;. CPRFTP generate <tasknumber> <cprnr> <keytext>

Once logged in, press the Enter key for a list of available commands.

##1. FTP

CPR supports three file transfer protocols (FTP):

1. SFTP (port **2222**)
2. FTPS explicit liabilities (port **321** + data ports from **50,000 to 52,000**)
3. FTPS implicit liabilities (port **990** + data ports from **50,000 to 52,000**)

The host addresses for all FTP endpoints are:

* Production: **ftp.cpr.dk (127.29.101.4)**
* Test: **ftp-demo.cpr.dk (127.29.101.26)**

In order to login to the CPR FTP server, you must use the user credentials supplied to you by the CPR office.

After logginng in to the FTP server, you will be placed in your user's home directory, which contains two subfolders:

* in
* ud

The "in" directory is intended for input files to the CPR system. The "ud" directory is intended for output files from the CPR system which can be used by your organisation.

File names in the "in" directory must follow a naming convention, specifically:

    dYYMMDD.iCCCCNN

    e.g. d151021.i999988

File names in the "ud" directory must follow a naming convention, specifically:

    dYYMMDD.lCCCCNN

    e.g. d151021.l999988
    
Where:

* "d" is the character literal "d" - a constant
* "YYMMDD" is the current date
* "i" is the character literal "i" - a constant (in files) / "l" is the character literal "l" - a constant (out files)
* "CCCC" is your customer number, provided to you by the CPR office
* "NN" is the last two digits of the extract's task number (opgavenummer)