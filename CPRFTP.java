/*
 * Copyright (c) 2015 CSC Denmark A/S
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 *
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 *
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

import org.apache.commons.net.ftp.*;
import org.apache.commons.net.util.TrustManagerUtils;
import java.io.*;
import java.util.Scanner;
import java.util.Calendar;
import java.text.SimpleDateFormat;

/**
 * This program demonstrates how FTP files can be downloaded from and uploaded to
 * CPR. Additionally, the example shows how input files can be generated, as well 
 * as how output files retrieved from CPR's FTP server can be parsed.
 *
 * For documentation regarding data contained in records, refer to "Teknisk dokumentation"
 * for "CPR Udtræk" on https://cprdocs.atlassian.net
 *
 * @author CSC
 */
public class CPRFTP {

    private final String SERVER = "ftp-demo.cpr.dk";
    private final int PORT = 990; // FTPS implicit port
    private static final String DOWNLOAD_DIR = "download/"; // default, can also point to other location
    private String workingDir = "/"; // when logging in, you start in your ftp user's root directory
    
    private String username;
    private FTPSClient ftps; // must have Apache Commons Net library on classpath

    /**
     * Main program logic.
     *
     * @param args Arguments needed to authenticate with FTP server. Username and password
     * should be passed to program in order to connect to CPR FTP server, or 'parse <exportfile>'
     * to parse downloaded export file.
     * @throws Exception We throw a general Exception to keep the example clean, 
     * otherwise specific exceptions should be caught and handled according to your
     * own program logic.
     */
    public static void main(String[] args) throws Exception {
        if (args.length < 2) {
            System.out.println("Missing arguments. Usage:");
            System.out.println("$ java CPRFTP <ftpuser> <password>");
            System.out.println("$ java CPRFTP parse <exportfile>");
            System.out.println("$ java CPRFTP generate <tasknumber> <cprnr> <keytext>");
            return;
        }
        
        if (args[0].equalsIgnoreCase("parse")) {
            // parse file instead of connect to CPR FTP server
            ExportParser.parse(args[1]);
            return;
        }
        
        if (args[0].equalsIgnoreCase("generate")) {
            if (args.length < 4) {
                System.out.println("Too few arguments to proceed.");
                return;
            }
            // parse file instead of connect to CPR FTP server
            ImportGenerator.create(args[1], args[2], args[3]);
            return;
        }
        
        CPRFTP example = new CPRFTP(args[0]);

        System.out.println("Starting FTPS client...");

        if (example.initFTPClient()) {
            System.out.println("Connected.");
            if (example.login(args[1]))
            {
                example.loopUntilDisconnect(); // process user commands until they quit
            } else {
                System.err.println("Credentials denied.");
            }
        } else {
            System.err.println("Could not initialize FTPSClient.");
        }
        example.cleanup();
        System.out.println("Goodbye.");
    }
    
    /**
     * Constructor for CPRFTP example.
     *
     * @param username The username used to login to the FTP server.
     */
    public CPRFTP(String username) {
        this.username = username;
    }
    
    /**
     * Initializes FTPS client with security settings and other configuration
     * parameters necessary to connect to CPR. 
     *
     * @return Returns true is connection to CPR was success, false if the connection
     * could not succeed. 
     */
    public boolean initFTPClient() throws Exception {
        ftps = new FTPSClient(true); // implicit FTPS
        
        // may be secure or insecure, depending on how the CA is configured on the server
        ftps.setTrustManager(TrustManagerUtils.getValidateServerCertificateTrustManager());
        ftps.connect(SERVER, PORT);
        
        int replyCode = ftps.getReplyCode();
        return FTPReply.isPositiveCompletion(replyCode);
    }
    
    /**
     * Logs in to FTP server using supplied username and password.
     * 
     * @param password The password used to login to the FTP server
     * @return Returns true if login was successful, false on failure.
     * @throws Exception
     */
    public boolean login(String password) throws Exception {
        if (ftps.login(username, password)) {
            System.out.println("Logged in.");
            System.out.println("Remote system is: " + ftps.getSystemType());
            System.out.println("Working directory is: " + ftps.printWorkingDirectory());
            return true;
        }
        
        return false;
    }
    
    /**
     * Login to FTP server using supplied username and password.
     *
     * @throws Exception 
     */
    public void loopUntilDisconnect() throws Exception {
        Scanner userInput = new Scanner(System.in);
        String[] inCmds;
        String cmd;
        boolean running = true; // so program knows when to stop looping
        
        ftps.setFileType(FTP.ASCII_FILE_TYPE); // so line endings are handled correctly
        ftps.enterLocalPassiveMode();
        ftps.setListHiddenFiles(true); // show ./ and ../
        
        do {
            String toSendFile = null;
            String cdDir = null;
            String getFile = null;

            System.out.print(String.format("%s@%s%s $ ", username, SERVER, workingDir));
            inCmds = userInput.nextLine().split(" "); // get user input
            cmd = inCmds[0]; // get first command typed by user

            if (inCmds.length == 2)
                toSendFile = cdDir = getFile = inCmds[1]; // set some variables to point to 2nd user argument

            switch (cmd) {
                case "cd":
                    // change directory on FTP server
                    if (cdDir != null) {
                        // change directory to the one the user wants to list (optional argument)
                        ftps.changeWorkingDirectory(cdDir);
                        workingDir = ftps.printWorkingDirectory();
                    } else {
                        System.err.println("No directory provided. Use 'cd <directory>' to change to directory.");
                    }
                    break;
                case "put":
                    // upload a file to FTP server
                    if (toSendFile != null) {
                        System.out.println(String.format("Saving file '%s' in directory: %s", toSendFile, ftps.printWorkingDirectory()));
                        InputStream input;

                        input = new FileInputStream(toSendFile);
                        ftps.storeFile(workingDir + "/" + toSendFile, input);

                        input.close();
                        System.out.println("File stored on server: " + toSendFile);
                    } else {
                        System.out.println("No file provided. Use 'put <file>' to send file.");
                    }
                    break;
                case "get":
                    // download a file from FTP server
                    if (getFile != null) {
                        OutputStream output;
                        output = new FileOutputStream(DOWNLOAD_DIR + getFile);

                        ftps.retrieveFile(workingDir + "/" + getFile, output);

                        output.close();
                        System.out.println("Downloaded file: " + getFile);
                    } else {
                        System.out.println("No file provided. Use 'get <file>' to download file.");
                    }
                    break;
                case "ls":
                    // list contents of current directory on FTP server
                    System.out.println(String.format("Contents of '%s':", ftps.printWorkingDirectory()));

                    for (FTPFile dir : ftps.listDirectories()) {
                        System.out.println(String.format("     %s/", dir.getRawListing()));
                    }

                    for (FTPFile f : ftps.listFiles()) {
                        if (!f.isDirectory())
                            System.out.println(String.format("     %s", f.getRawListing()));
                    }
                    break;
                case "logout":
                case "exit":
                case "quit":
                    // exit command loop
                    running = false;
                    break;
                default:
                    System.err.println(String.format("Valid commands are: cd, ls, get, put", cmd));

            }
        } while (running);
        ftps.logout();
    }
    
    /**
     * Performs any necessary cleanup such as cleanly closing connections.
     *
     * @throws Exception 
     */
    public void cleanup() throws Exception {
        ftps.disconnect();
    }
    
    
    /**
     * Utility class for parsing export files retrieved from CPR. Export files follow
     * a predefined, fixed-length format as defined in the "uddataformat" specifications.
     * 
     * This example shows how to parse "ændringsudtræk"-formatted exports.
     */
    static class ExportParser {
        /**
         * Parses a file retrieved from CPR FTP server and displays data from received
         * records. 
         * 
         * @param filename Name of file to parse.
         * @throws Exception
         */
        public static void parse(String filename) throws Exception {
            System.out.println("Parsing: " + DOWNLOAD_DIR + filename);
            BufferedReader file = new BufferedReader(new FileReader(DOWNLOAD_DIR + filename));
            
            String line;
            while ( (line = file.readLine()) != null ) {
                // each line in export file represents one record
                ExportParser.parseRecord(line);
            }
        }
        
        /**
         * Parses a record from export file.
         * 
         * @param record The person data record to parse.
         * @throws Exception 
         */
        public static void parseRecord(String record) throws Exception {
            String recordType = record.substring(0, 3); // recordType is always first 3 characters of line
        
            // handle records based on their type
            if (recordType.equals("000")) { // START record
                // handle record
            } else if (recordType.equals("001")) { // CURRENT_DATA record
                // parse this record for demonstration purposes
                ExportParser.printCurrentData(record);
            } else if (recordType.equals("002")) { // FOREIGN_ADDRESS record
                // handle record
            } else if (recordType.equals("003")) { // KONTAKT_ADDRESS record
                // handle record
            } else if (recordType.equals("004")) { // MARITAL_STATUS record
                // handle record
            } else if (recordType.equals("005")) { // GUARDIAN record
                // handle record
            } else if (recordType.equals("011")) { // CUSTOMERNUM_REF record
                // handle record
            } else if (recordType.equals("999")) { // END record
                // handle record
            } else {
                System.out.println("Unknown record code: " + recordType);
            }
        }
        
        /**
         * Print current data record for given person.
         *
         * @param record The record to handle.
         * @throws Exception 
         */
        public static void printCurrentData(String record) throws Exception {
            // start position and length from export specification
            // due to Java 0-indexing in strings, subtract 1 from 'Pos.' as shown in record specification
            
            int recordStart = 0;
            String givenName = subStr(record, recordStart + 332, 50).trim();    // FORMELNVN field in position 333, length 50
            String lastName = subStr(record, recordStart + 382, 40).trim();     // EFTERNVN field in position 383, length 40
            String cprNr = subStr(record, recordStart + 3, 10);                 // PNR field in position 4, length 10
            String sex = subStr(record, recordStart + 21, 1);                   // KOEN field in position 22, length 1
            String birthdate = subStr(record, recordStart + 13, 8);             // FOEDDTO field in position 14, length 8
            String address = subStr(record, recordStart + 218, 34).trim();      // STANDARDADR field in position 219, length 34
            
            // check for data before printing
            System.out.println("Given name: " + givenName);
            System.out.println("Last name: " + lastName);
            System.out.println("CPRnr: " + cprNr);
            System.out.println("Gender: " + sex);
            System.out.println("Birthdate: " + birthdate);
            if (!address.equals(""))
                System.out.println("Address: " + address);
        }
    }
    
    /**
     * Example input file generator that creates files for upload to CPR's FTP server.
     */
    static class ImportGenerator {
        
        /**
         * Creates a new input file that can be uploaded to the CPR FTP server. This
         * example uses the import data file structure specified in 
         * "Udtræksvejledning for private brugere", which can be found on
         * https://cprdocs.atlassian.net 
         *
         * @param taskNumber The "opgavenummer" provided to you by the CPR office.
         * @param cprNr The CPR number of the person this record describes.
         * @param key Text that can be specified for own use. Must be agreed upon with CPR office.
         * @throws Exception
         */
        public static void create(String taskNumber, String cprNr, String key) throws Exception {
            // get current date in YYMMDD format, required for filename format
            String date = new SimpleDateFormat("yyMMdd").format(Calendar.getInstance().getTime());
             
             // file must be using ISO-8859-1 encoding, and the filename must follow specific format
            PrintWriter file = new PrintWriter(
                String.format("d%s.i%s", date, taskNumber), "ISO-8859-1");
            
            // personnummer-nøgle
            file.println(String.format("%-80.80s", // record must be 80 characters in length
                String.format("%s%s%s%15.15s%s",
                    "01", // constant for type "personnummer-nøgle"
                    taskNumber,
                    cprNr,
                    key,
                    "" // blank - constant
                )));
                
            // personnummer-abonnement - create
            file.println(String.format("%-80.80s", // record must be 80 characters in length
                String.format("%s%s%s%s%s%15.15s%s",
                    "06", // constant for type "personnummer-abonnement"
                    "0266", // kundenummer
                    "00", // konstant
                    "OP", // SL = remove (slette), OP = create (oprette)
                    cprNr,
                    key, // must agree on text with CPR office
                    "" // blank - constant
                )));
                
            // personnummer-abonnement - remove
            file.println(String.format("%-80.80s", // record must be 80 characters in length
                String.format("%s%s%s%s%s%15.15s%s",
                    "06", // constant for type "personnummer-abonnement"
                    "0266", // kundenummer
                    "00", // konstant
                    "SL", // SL = remove (slette), OP = create (oprette)
                    cprNr,
                    key, // must agree on text with CPR office
                    "" // blank - constant
                )));
                
            // personnummer-rekvisition
            file.println(String.format("%-80.80s", // record must be 80 characters in length
                String.format("%s%s%s%15.15s%s",
                    "07", // constant for type "personnummer-rekvisition"
                    taskNumber,
                    cprNr,
                    key, // must agree on text with CPR office
                    "" // blank - constant
                )));
                
            file.close(); // file is written to current working directory on local machine
        }
    }
    
    /**
     * Helper method to return part of a string, starting at a given index up
     * to a specified length.
     * 
     * @param str The String object to get a substring from
     * @param index The 0-based index in the String object to start from
     * @param length The length of the substring to be returned
     * @return Returns a new String object
     */
    public static String subStr(String str, int index, int length) {
        return str.substring(index, index + length);
    }
}